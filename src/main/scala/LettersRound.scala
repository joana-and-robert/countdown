import worker.LettersWorker.{getLongestWordsInBoundary, isWordInBoundary, getLetter, isWordValid}

import scala.annotation.tailrec
import scala.io.StdIn.readLine

object LettersRound extends Round{

  def play(p1: Player, p2: Player): Unit = {
    val boundary = generateBoundary()
    println(boundary)

    queryPlayers(p1, p2, boundary)
    println(getLongestWordsInBoundary(boundary))
  }

  def queryPlayers(p1: Player, p2: Player, boundary: Seq[Char]): Unit = {
    val word1 = readLine(s"${p1.name}, enter your word: ")
    val word2 = readLine(s"${p2.name}, enter your word: ")

    val p1score = calculateScore(word1, boundary)
    val p2score = calculateScore(word2, boundary)

    if (p1score == p2score) {
      p1.score += p1score
      p2.score += p2score
    }
    else if (p1score > p2score) {
      p1.score += p1score
    }
    else if (p2score > p1score) {
      p2.score += p2score
    }
  }

  def calculateScore(word: String, boundary: Seq[Char]): Int = {
    if (isWordInBoundary(word, boundary) && isWordValid(word)) {
      if (word.length == 9) 18
      else word.length
    }
    else 0
  }

  def generateBoundary(): Seq[Char] = {
    @tailrec
    def generateBoundaryAcc(vCount: Int, cCount: Int, boundary: Seq[Char]): Seq[Char] = {
      if (boundary.length >= 9) boundary
      else if (cCount < 6 && vCount < 5) {
        val selection = readLine("Enter v for vowel, or c for consonant: ").toLowerCase()
        val char = getLetter(selection)

        selection(0) match {
          case 'v' => generateBoundaryAcc(vCount + 1, cCount, boundary :+ char)
          case 'c' => generateBoundaryAcc(vCount, cCount + 1, boundary :+ char)
          case _ => generateBoundaryAcc(vCount, cCount, boundary)
        }

      }
      else if (vCount >= 5) {
        val char = getLetter("consonant")
        generateBoundaryAcc(vCount, cCount + 1, boundary :+ char)
      }
      else if (cCount >= 6) {
        val char = getLetter("vowel")
        generateBoundaryAcc(vCount + 1, cCount, boundary :+ char)
      }
      else
        boundary
    }

    generateBoundaryAcc(0, 0, Seq[Char]())
  }
}