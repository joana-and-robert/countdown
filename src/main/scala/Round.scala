abstract class Round {
  def play(p1: Player, p2: Player): Unit
}
