import scala.annotation.tailrec
import scala.io.StdIn.readLine
import scala.util.Random.{nextFloat, shuffle}


def playGame(p1: Player, p2: Player, roundNumber: Int, maxRound: Int): Unit = {
  if (roundNumber == maxRound) {
    // conundrum round
    println(s"Round $roundNumber: Conundrum Round")
    ConundrumRound.play(p1, p2)

    printScores(p1, p2)
    playGame(p1, p2, roundNumber + 1, maxRound)
  }
  else {
    if (roundNumber > maxRound) {
      // show winner and end game
      println("Fin.")

      val winner = if (p1.score > p2.score) p1.name else if (p1.score < p2.score) p2.name else ""
      if (winner == "")
        println("Draw!")
      else
        println(s"$winner won!")

      printScores(p1, p2)
    } else {
      roundNumber % 3 == 0 match {
        case true => {
          // numbers round
          println(s"Round $roundNumber: Numbers Round")
          NumbersRound.play(p1, p2)

          printScores(p1, p2)
          playGame(p1, p2, roundNumber + 1, maxRound)
        }
        case false => {
          // letters round
          println(s"Round $roundNumber: Letters Round")
          LettersRound.play(p1, p2)

          printScores(p1, p2)
          playGame(p1, p2, roundNumber + 1, maxRound)
        }
      }
    }
  }
}

@main
def main(): Unit = {
  printIntro()


  val player1 = new Player(readLine("Player 1, Enter your name:  "))
  val player2 = new Player(readLine("Player 2, Enter your name:  "))

  playGame(player1, player2, 1, 4)
}

def printIntro(): Unit = {
  println("CountDown: May The Odds Ever Be In Your Favour!")
  //  println(
  //    """
  //      |   ____                  _   ____
  //      |  / ___|___  _   _ _ __ | |_|  _ \  _____      ___ __
  //      | | |   / _ \| | | | '_ \| __| | | |/ _ \ \ /\ / / '_ \
  //      | | |__| (_) | |_| | | | | |_| |_| | (_) \ V  V /| | | |
  //      |  \____\___/ \__,_|_| |_|\__|____/ \___/ \_/\_/ |_| |_| _       ____
  //      | |  \/  | __ _ _   _  | |_| |__   ___   / _ \  __| | __| |___  | __ )  ___
  //      | | |\/| |/ _` | | | | | __| '_ \ / _ \ | | | |/ _` |/ _` / __| |  _ \ / _ \
  //      | | |  | | (_| | |_| | | |_| | | |  __/ | |_| | (_| | (_| \__ \ | |_) |  __/
  //      | |_|__|_|\__,_|\__, |  \__|_| |_|\___|  \___/ \__,_|\__,_|___/_|____/ \___|           _
  //      | | ____|_   ___|___/__  |_ _|_ __   \ \ / /__  _   _ _ __  |  ___|_ ___   _____  _ __| |
  //      | |  _| \ \ / / _ \ '__|  | || '_ \   \ V / _ \| | | | '__| | |_ / _` \ \ / / _ \| '__| |
  //      | | |___ \ V /  __/ |     | || | | |   | | (_) | |_| | |    |  _| (_| |\ V / (_) | |  |_|
  //      | |_____| \_/ \___|_|    |___|_| |_|   |_|\___/ \__,_|_|    |_|  \__,_| \_/ \___/|_|  (_)
  //      |
  //      |""".stripMargin)
}

def printScores(p1: Player, p2: Player): Unit = {
  println(s"${p1.name}: ${p1.score}")
  println(s"${p2.name}: ${p2.score}")
  println()
  //  println(s"${p1.name}")
  //   println(s"""
  //   ┌─┐
  //   │${p1.score}│
  //   └─┘
  //   """)
  //  println(s"${p2.name}")
  //  println(
  //    s"""
  //  ┌─┐
  //  │${p2.score}│
  //  └─┘
  //  """)
}

