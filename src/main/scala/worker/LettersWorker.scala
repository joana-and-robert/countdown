package worker.LettersWorker{
  import scala.annotation.tailrec
  import scala.util.Random.shuffle

  val lines = io.Source.fromFile("src/main/scala/resources/valid_words.txt")
  val validWords = (for (line <- lines.getLines()) yield line.toUpperCase()).toSet
  lazy val nineLetterWords = validWords.filter(x => x.length == 9).toVector

  val vowels = List('A', 'E', 'I', 'O', 'U')
  val consonants = 'A' to 'Z' filter (x => !(vowels contains (x)))

  def isWordValid(word: String): Boolean = {
    validWords contains word.toUpperCase()
  }

  def getLetter(selection: String): Char = {
    selection match {
      case "vowel" | "v" => shuffle(vowels).head
      case "consonant" | "c" => shuffle(consonants).head
    }
  }

  def isWordInBoundary(word: String, boundary: Seq[Char]): Boolean = {
    val word1 = word.toUpperCase()

    @tailrec
    def validAcc(word: String, boundary: Seq[Char]): Boolean = {
      if (word.isEmpty) true
      else if (boundary.length <= 0) false
      else
        val index = boundary.indexOf(word.head)

        if (index < 0) false
        else validAcc(word.tail, boundary.slice(0, index) ++ boundary.slice(index + 1, boundary.length))

    }

    validAcc(word1, boundary)
  }

  def getLongestWordsInBoundary(boundary: Seq[Char]): List[String] = {
    val possibleWords = validWords.filter(isWordInBoundary(_, boundary)).toList
    
    val longestWords = possibleWords.sortWith((a, b) => a.length > b.length)
    val longestX = longestWords.filter(_.length == longestWords(0).length)
    
    longestX
  }
}
