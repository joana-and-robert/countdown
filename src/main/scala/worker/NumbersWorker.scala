package worker.NumbersWorker {
  import scala.annotation.tailrec
  import scala.collection.mutable.Stack
  import scala.util.Random

  val largeNumbers = List(25, 50, 75, 100)
  val smallNumbers = (1 to 10) ++ (1 to 10)

  def generateRoundThreeDigit(): Int = {
    Random.between(100, 1000)
  }

  def generateNumbers(numLarge: Int, numSmall: Int): List[Int] = {
    val (useLarge, unusedLarge) = Random.shuffle(largeNumbers).splitAt(numLarge)
    val (useSmall, unusedSmall) = Random.shuffle(smallNumbers).splitAt(numSmall)

    useLarge ++ useSmall ++ Random.shuffle(unusedLarge ++ unusedSmall).take(6 - (numLarge + numSmall))
  }

  def isNumbersInBoundary(input: String, boundary: Seq[Int]): Boolean = {
    val userInts = for (char <- input.split("[+-/*()]") if !char.trim().isBlank) yield char.trim().toInt

    @tailrec
    def isInputValidAcc(nums: Seq[Int], boundary: Seq[Int]): Boolean = {
      if (nums.isEmpty) true
      else if (boundary.length <= 0) false
      else
        val index = boundary.indexOf(nums.head)

        if (index < 0) false
        else isInputValidAcc(nums.tail, boundary.slice(0, index) ++ boundary.slice(index + 1, boundary.length))

    }

    isInputValidAcc(userInts, boundary)
  }

  def eval(expr: String): Int = {
    val operands = Stack[Int]()
    val operators = Stack[Char]()

    val ops = raw"[-+/*]".r
    val nums = raw"\d+".r

    val precedence = Map('*' -> 2, '/' -> 2, '+' -> 1, '-' -> 1)

    def compute(): Int = {
      val a = operands.pop()
      val b = operands.pop()
      val sym = operators.pop()

      sym match {
        case '+' => a + b
        case '-' => b - a
        case '*' => a * b
        case '/' => b / a
      }
    }

    var i = 0
    while (i < expr.length) {
      var char = expr.charAt(i)
      if (nums.matches(s"$char")) {
        var str = ""
        while (i < expr.length && nums.matches(s"$char")) {
          str += char
          i += 1
          if (i < expr.length) char = expr.charAt(i)
        }
        i -= 1
        operands.push(str.toInt)
      }
      else if (char == '(') operators.push(char)
      else if (char == ')') {
        while (operators(0) != '(') {
          val ans = compute()
          operands.push(ans)
        }
        operators.pop()
      }
      else if (ops.matches(s"$char")) {
        if (operators.isEmpty) operators.push(char)
        else {
          val pCurr = precedence.getOrElse(char, 0)
          val pPrev = precedence.getOrElse(operators(0), 0)

          if (pCurr >= pPrev) operators.push(char)
          else {
            while (operators.nonEmpty && pCurr <= precedence.getOrElse(operators(0), 0)) {
              val ans = compute()
              operands.push(ans)
            }
            operators.push(char)
          }
        }
      }

      i += 1
    }


    while (operators.nonEmpty) {
      val ans = compute()
      operands.push(ans)
    }
    operands.pop()
  }
}
