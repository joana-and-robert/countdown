import worker.NumbersWorker.{generateRoundThreeDigit, eval, generateNumbers, isNumbersInBoundary}

import scala.annotation.tailrec
import scala.io.StdIn.readLine


object NumbersRound extends Round {
  def play(p1: Player, p2: Player): Unit = {
    val numLarge = getUserInput(4, "large")
    val numSmall = getUserInput(6-numLarge, "small")

    val boundary = generateNumbers(numLarge, numSmall)
    val target = generateRoundThreeDigit()

    println(s"boundary: $boundary")
    println(s"target: $target")

    val arith1 = readLine(s"${p1.name}, enter your maths: ")
    val arith2 = readLine(s"${p2.name}, enter your maths: ")

    val p1score = calculateScore(arith1, boundary, target)
    val p2score = calculateScore(arith2, boundary, target)

    p1.score += p1score
    p2.score += p2score

  }

  def calculateScore(str: String, boundary: Seq[Int], target: Int): Int = {
    val isValid = isNumbersInBoundary(str, boundary)

    if (!isValid) 0
    else {
      val ans = eval(str)
      println(ans)

      if (ans == target) 10
      else if (target - 5 <= ans && ans <= target - 1) 7
      else if (target - 10 <= ans && ans <= target - 6) 5
      else 0
    }
  }

  def getUserInput(upperBound: Int, prompt: String): Int = {
    val input = readLine(s"number of $prompt: ").toInt

    if (0<= input && input <= upperBound) input
    else {
      println(s"invalid input. input must be <=0 and <=$upperBound")
      getUserInput(upperBound, prompt)
    }
  }
}

