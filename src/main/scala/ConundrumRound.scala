import worker.LettersWorker.{nineLetterWords}

import scala.annotation.tailrec
import scala.io.StdIn.readLine
import scala.util.Random
import scala.util.Random.shuffle


object ConundrumRound extends Round {
  def play(p1: Player, p2: Player): Unit = {
    val bigWord = generateWord()

    val shuffledWord: Seq[Char] = bigWord._1
    val actualWord: String = bigWord._2

    println(shuffledWord)
//    println(actualWord)

    var gotItRight = false
    val (currentPlayer, nextPlayer) = if(p1.name == getValidName(p1.name, p2.name))
      Tuple2(p1, p2) else Tuple2(p2, p1)

    val word = readLine(s"${currentPlayer.name}, enter your word: ")
    if(isWordCorrect(word, actualWord)) {
      currentPlayer.score += 10
      gotItRight = true
      println(s"${currentPlayer.name} got it right!")
    }
    else {
      println(s"${currentPlayer.name} got it wrong!")

      val nextPlayerWord = readLine(s"${nextPlayer.name}, enter your word: ")
      if (isWordCorrect(nextPlayerWord, actualWord)) {
        nextPlayer.score += 10
        gotItRight = true
        println(s"${nextPlayer.name} got it right!")

      }else println(s"${nextPlayer.name} got it wrong!")
    }

    if (!gotItRight) {
      println(s"${shuffledWord.mkString("")} => $actualWord")
    }
  }

  def generateWord(): (Seq[Char], String) = {
    val randIndex = Random.nextInt(nineLetterWords.length)
    val randWord = nineLetterWords(randIndex)

   Tuple2(shuffle(randWord).toVector, randWord)
  }

  def isWordCorrect(word: String, actualWord: String): Boolean = {
    if (word.toUpperCase() == actualWord.toUpperCase()) true
    else false
  }

  def getValidName(validNames: String*): String = {
    val name = readLine("Enter your name to answer: ")

    if(validNames contains name) name
    else {
      println("Invalid player name.")
      getValidName(validNames: _*)
    }
  }
}
